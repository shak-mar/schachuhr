# schachuhr

> A clock for playing time-limited chess

Simply run `./schachuhr.sh` in a large-enough terminal.  Pressing any key
switches the timer.  To modify default values (5 minutes each) simply edit the
script.
